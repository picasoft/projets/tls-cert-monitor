# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    tls-certs-monitor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tls-certs-monitor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tls-certs-monitor.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

"""Implements Docker action classes.

Classes
-------
    DockerAction
        Base class for all docker action classes

    RestartDockerAction
        An action that restarts a cointainer

    KillDockerAction
        An action that sends a signal (by default SIGHUP) to a running container
"""

import docker
import logging

logger = logging.getLogger(__name__)

class DockerAction:
    """Base class for all docker action classes.

    Attributes
    ----------
        client 
            Docker API Client object that is used to run the action
        container_id
            ID of the target container

    Methods
    -------
        exec()
            Executes the action on the target container

    """

    def __init__(self, docker_client, container_id):
        """
        Parameters
        ----------
            docker_client : DockerClient
                Docker API client object that is used to run the action
            container_id : int
                ID of the target container
        """
        self.client = docker_client
        self.container_id = container_id
        pass

    def exec(self):
        """Checks if the target container is running and executes the action
        """
        c = self.client.containers.get(self.container_id)
        if c.status == 'running':
            self.action(c)

    def action(self, container):
        """Executes the action

        This method is meant to be overriden in child classes.

        Parameters
        ----------
            container : Container
                The target container
        """
        pass

class RestartDockerAction(DockerAction):
    """This class implements a restart docker action
    """
    def __init__(self, docker_client, container_id):
        super().__init__(docker_client, container_id)

    def action(self, container):
        try:
            logger.info('Restarting container {0}'.format(container.name))
            container.restart()
        except docker.errors.APIError:
            logger.error('Unable to restart container {0}({1})'.format(container.name,
                container.id))

class KillDockerAction(DockerAction):
    """This class implements a kill docker action

    Attributes
    ----------
        signal : str
            The signal to use when killing the container
    """
    def __init__(self, docker_client, container_id, signal='SIGHUP'):
        super().__init__(docker_client, container_id)
        self.signal = signal

    def action(self, container):
        try:
            logger.info('Killing container {0} with signal {1}'.format(container.name, self.signal))
            container.kill(self.signal)
        except docker.errors.APIError:
            logger.error('Unable to kill container {0}({1}) with signal {2}'
                .format(container.name, self.container_id, self.signal))