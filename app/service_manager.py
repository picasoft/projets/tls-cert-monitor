# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    tls-certs-monitor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tls-certs-monitor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tls-certs-monitor.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

"""Handling of docker services

This module provides a ServicesManager class implementing a collection of
services whose certificates are to be updated by the ssytem.

Classes
-------
    Service
        A class representing a single service

    ServiceManager
        A class handling a collection of services
"""

from threading import Lock

class Service:
    """
    A service wrapper This object represents a docker service

    Attributes
    ----------

    id : int
        The id of the container running the service
    host : str
        The hostname associated with the service used for CN in TLS certs
    action : DockerAction
        An action to perform on the service when the run_action method is
        invoked.
    owner_uid : int
        The file owner's uid. Defaut to 0 (root)
    Methods
    -------

    run_action()
        Runs the action bound to this service
    """

    def __init__(self, id, host, action, owner_uid=0):
        """
        Parameters
        ----------
        id : int
            The id of the container running the service
        host : str
            The hostname associated with the service used for CN in TLS certs
        action : DockerAction
            An action to perform on the service when the run_action method is invoked.
        """

        self.id = id
        self.host = host
        self.action = action
        self.owner_uid = owner_uid

    def run_action(self):
        """Executes the action on the container associated to the service."""
        if self.action is not None:
            self.action.exec()

class ServicesManager:
    """
    A ServicesManager handles a collection of services and forwards certicate update
    requests to a CertUpdater. If a service certificate is updated (either after adding a
    service or after an explicit update request), the docker action associated
    with the service is executed.

    Attribures
    ----------
        services_by_id : dict
            A dictionnary of the managed services identified by their container id.
        services_by_host : dict
            A dictionnary of the managed services identified by the CN of their
            certificates.
        updater : CertUpdater object
            The certificates updater in charge of handling the certificates for the managed
            services

    Methods
    -------
        add(service)
            Adds a new service to the collection

        remove(id)
            Removes a service from the collection based on its container id

        update()
            Checks if any certificate for a managed service needs to be updated. If
            so, performs the docker action associated with the service on its
            container
    """

    def __init__(self, updater):
        """
        Parameters
        ----------
            updater : CertUpdater
                The CertUpdater used to handle certificates for the services
        """
        self.services_by_id = {}
        self.services_by_host = {}
        self.updater = updater
        self.lock = Lock()

    def add(self, service):
        """ Adds a service to the collection and enables handling of its certificates
        """
        with self.lock:
            # add could be called 2 times : create and start events
            # don't monitor the same service two times
            if service.id in self.services_by_id:
                return
            self.services_by_id[service.id] = service
            if service.host not in self.services_by_host:
                self.services_by_host[service.host] = [service]
            else:
                self.services_by_host[service.host].append(service)

            self.updater.add(service.host, service.owner_uid)
        self.update()

    def remove(self, id):
        """ Removes a service by its id from the collection and disables handling
        of its certificates
        """
        with self.lock:
            if id in self.services_by_id:
                service = self.services_by_id[id]
                host = service.host
                self.services_by_host[host].remove(service)
                del self.services_by_id[id]
                if len(self.services_by_host[host]) == 0:
                    del self.services_by_host[host]
                    self.updater.remove(host)

    def update(self):
        """ Updates the certificates of the services if needed. If a certificate
        has changed, exectute the associated docker action on the service container.
        """
        with self.lock:
            for h in self.updater.update():
                for s in self.services_by_host[h]:
                    s.run_action()
