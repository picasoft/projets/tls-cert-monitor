# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    tls-certs-monitor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tls-certs-monitor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tls-certs-monitor.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

"""Docker event monitoring

Provides a monitor for docker events that add/remove services when
they occur according to the configuration provided in the labels of these
services.

Classes
-------
    DockerMonitor
        A monitor for docker events.
"""

import docker
import threading
import requests
import logging
import re

from service_manager import Service
from docker_actions import RestartDockerAction
from docker_actions import KillDockerAction

logger = logging.getLogger(__name__)

class DockerMonitor(threading.Thread):
    """Implements a docker events monitoring thread.

    This class implements a thread that listen to the docker socket to intercept
    events on specific containers to launch actions on these when start and stop events occur.
    Containers that should be monitored are identified by labels.

    """
    def __init__(self, services_mgr):
        self.docker_client = docker.from_env()
        self.events = None
        self.services_mgr = services_mgr
        super().__init__()

    def run(self):
        logger.debug('Starting Docker events monitor')
        # Identify all running containers and get dependent services
        # at startup
        try:
            containers = self.docker_client.containers.list(all)

            for c in containers:
                self._add_service(c)

            # Then, wait for docker events and identify new dependent services
            # or services that exit the system. Do an update each time a new
            # service is added / restarted etc. or a service is changed.
            self.events = self.docker_client.events(decode=True)
            for event in self.events:
                if 'status' not in event:
                    continue

                try:
                    c = self.docker_client.containers.get(event['id'])
                    if event['status'] == 'stop':
                        self._remove_service(c)
                    elif event['status'] in ['start', 'create']:
                        self._add_service(c)

                except docker.errors.NotFound:
                    pass
                except docker.errors.APIError as error:
                    logger.error('Docker error while looking up container {0}: {1} '.format(event['id'], error.strerror))
        except requests.exceptions.ConnectionError as error:
            logger.error('Connection to docker socket refused.')
        logger.debug('No more events to handle: stopping monitor')

    def stop(self):
        if self.events:
            self.events.close()
            self.join()

    def _get_host_from_traefik_rule(self, container):
        # Traefik v1
        if 'traefik.frontend.rule' in container.labels:
            try:
                return container.labels['traefik.frontend.rule'].split('Host:')[1].split(',')[0].split(';')[0].strip()
            except IndexError:
                logger.error("Bad format for traefik.frontend.rule")
                return None
        # Traefik v2
        p = re.compile('traefik.http.routers.*.rule')
        for label, value in container.labels.items():
            if p.match(label):
                urls = re.search('Host\(`(.*?)`\)', value)
                if urls:
                    try:
                        return urls.group(1).split(',')[0].strip()
                    except IndexError:
                        logger.error("Bad format for traefik.http.routers.*.rule")
                        return None
                else:
                    logger.error("Bad format for traefik.http.routers.*.rule")
                    return None

    def _get_uid_from_label(self, container):
        if 'tls-certs-monitor.owner' in container.labels:
            try:
                return int(container.labels['tls-certs-monitor.owner'])
            except ValueError:
                logger.error("Invalid value for tls-certs-monitor.owner, using 0")
                return 0
        return 0

    def _get_action_from_label(self, container):
        if 'tls-certs-monitor.action' not in container.labels:
            logger.info('No action defined for service {0}'.format(container.name))
            return None

        try:
            action = container.labels['tls-certs-monitor.action'].split(':')
            if action[0].strip() == 'kill':
                if len(action) == 1:
                    return KillDockerAction(self.docker_client, container.id, 'SIGHUP')
                else:
                    return KillDockerAction(self.docker_client, container.id, action[1].strip())
            elif action[0].strip() == 'restart':
                return RestartDockerAction(self.docker_client, container.id)
            else:
                return None

        except IndexError:
            logger.error('Invalid action specifier. Check tls-certs-monitor.action label for service {0}.'
                .format(container.name))
            return None

    def _add_service(self, container):
        if 'tls-certs-monitor.enable' in container.labels:
            if container.labels['tls-certs-monitor.enable'] in ['True', 'true']:
                host = self._get_host_from_traefik_rule(container)
                uid = self._get_uid_from_label(container)
            elif container.labels['tls-certs-monitor.enable'] in ['False', 'false']:
                pass
            else:
                logger.error('Invalid tls-certs-monitor.enable value for service {0}'
                      .format(container.name))

            if host:
                logger.info('Handling service {0} ({1})'.format(container.name, container.id))
                s = Service(container.id, host, self._get_action_from_label(container), uid)
                self.services_mgr.add(s)
        else:
            logger.debug('Skipping service {0} ({1})'.format(container.name, container.id))

    def _remove_service(self, container):
        self.services_mgr.remove(container.id)
