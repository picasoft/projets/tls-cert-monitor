# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    Foobar is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Foobar is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

""" Traefik Certificate extraction and update module

Provides tools to extract TLS certificates from Treafik acme.json files.

Classes
-------

    CertUpdater:
        This class handles acme files decoding and stores or updates the resulting
        certificates in separate folders.
"""

import os
import logging
import errno
import json
from base64 import b64decode
import binascii
from collections import defaultdict

logger = logging.getLogger(__name__)

class CertUpdater:
    """Decodes acme files and extract the resulting certificates if needed.

    This class keeps flat certificates for specified domain names
    in sync with the contents of an acme.json file. The certificates are stored in separated
    subdirectories named after the main domain name they are valid for.
    If the certificates specify x509 SAN records, only the main CN is used (a single
    directory is created, regardless of the number of alternate names.)

    Attributes
    ----------
        acme_file : str
            Name of the acme.json file containing the certificates
        names : tuple(str, int)
            List of domain names to keep updated along with owner UID
        certs_root_path : str
            Directory where the certificates will be stored. The certificates are
            stored in subdirectories of this directory

    Methods
    -------
        add(name)
            Adds a domain name to the updater

        remove(name)
            Removes a domain name from the updater

        update()
            Decodes the acme.json file and updates the certicates that need to be updated.



    """
    def __init__(self, acme_file, certs_root_path):
        """
        Parameters
        ----------
            acme_file : str
                path of the acme file containing the certificates
            certs_root_path : str
                root directory where certificates will be extracted.
                They are extracted in subdirectories named after the CN stored in
                the certificate.
        """
        self.acme_file = acme_file
        self.names = []
        self.certs_root_path = certs_root_path + '/'

    def add(self, name, owner=0):
        """Adds a domain name to the updater.

        Parameters
        ----------
            domain : str
                domain name to add
            owner : int
                destination owner of the files
        """

        self.names.append((name, owner))

    def remove(self, name):
        """Removes a domain name from the updater.

        Parameters
        ----------
            name : str
                domain name to remove
        """

        self.names = [x for x in self.names if x[0] != name]

    def update(self):
        """Decodes the acme.json file and updates the certicates that need to be updated.

        Returns
        -------
            list
                a list of domain names that have been updated. An empty list if no certificate
                has been updated.
        """
        # Open and decode the acme file
        try:
            with open(self.acme_file) as acme:
                data = json.loads(acme.read())
        except FileNotFoundError:
            logger.error('ACME file "{0}" not found when trying to decode.'.format(self.acme_file))
            return []
        except json.JSONDecodeError:
            logger.error('File "{0}" does not look like an ACME file.'.format(self.acme_file))
            return []

        # Guess Traefik version
        # Traefik v2 has a top level key per certificate provider
        traefik_v2 = 'Account' not in data.keys()

        # Get the acme file version
        acme_ver = 2
        try:
            # Traefik v2 always uses ACME v2
            if traefik_v2 or 'acme-v02' in data['Account']['Registration']['uri']:
                acme_ver = 2
        except TypeError:
            if 'DomainsCertificate' in data:
                acme_ver = 1

        # Get the certificates
        if acme_ver == 1:
            certs = data['DomainsCertificate']['Certs']
        elif acme_ver == 2:
            # If Traefik v2, merge all certificates
            if traefik_v2:
                certs = []
                for k, v in data.items():
                    if 'Certificates' in v.keys():
                        certs.extend(data[k]['Certificates'])
            else:
                certs = data['Certificates']

        # lower all keys, because in Traefik v2
        # they are and not in v1, even if ACME v2
        certs = self.lower_key(certs)

        # Iterate over certificates. If a certificate has been updated,
        # add its name to the updated_names.
        updated_names = []
        for c in certs:
            if acme_ver == 1:
                name = c['certificate']['domain']
                key = c['certificate']['privatekey']
                fullchain = c['certificate']['certificate']
            elif acme_ver == 2:
                name = c['domain']['main']
                key = c['key']
                fullchain = c['certificate']

            for (domain, uid) in self.names:
                # First check if the certificate domain matches a monitored domain name.
                # Otherwise, check if the certificate is a wildcard certificate applicable to the monitored domain (i.e. same parent subdomain)
                if name == domain or (name.startswith('*') and '.'.join(name.split('.')[1:]) == '.'.join(domain.split('.')[1:])):
                    try:
                        key = b64decode(key).decode('utf-8')
                        fullchain = b64decode(fullchain).decode('utf-8')
                        chain_start = fullchain.find('-----BEGIN CERTIFICATE-----', 1)
                        cert = fullchain[0:chain_start]
                        chain = fullchain[chain_start:]
                    except (UnicodeDecodeError, binascii.Error) as e:
                        logger.error('Error decoding certificates for {0}.'.format(name))
                        logger.error(e)
                        continue
                    logger.info('Updating certificates for "{0}"'.format(domain))
                    # Create directories etc if they do not exist
                    path = self.certs_root_path + domain + '/'
                    try:
                        os.makedirs(path)
                    except OSError as error:
                        if error.errno != errno.EEXIST:
                            raise

                    if self._needs_updating(domain, fullchain):
                        priv_path = path + 'privkey.pem'
                        with open(priv_path, 'w') as f:
                            f.write(key)
                        os.chmod(priv_path, 0o600)
                        os.chown(priv_path, uid, -1)

                        cert_path = path + 'cert.pem'
                        with open(cert_path, 'w') as f:
                            f.write(cert)
                        os.chown(cert_path, uid, -1)

                        chain_path = path + 'chain.pem'
                        with open(chain_path, 'w') as f:
                            f.write(chain)
                        os.chown(chain_path, uid, -1)

                        fullchain_path = path + 'fullchain.pem'
                        with open(fullchain_path, 'w') as f:
                            f.write(fullchain)
                        os.chown(fullchain_path, uid, -1)

                        logger.info('Certificates for "{0}" updated'.format(domain))

                        updated_names.append(domain)
                    else:
                        logger.info('Certificates for "{0}" are already up-to-date'.format(domain))

        return updated_names

    def _needs_updating(self, name, fullchain):
        """Checks if a certificate has changed

        Parameters
        ----------
            name : str
                Name of the directory containing the certificates
            fullchain : str
                Full certificates extracted from the acme.json file

        Returns
        -------
            bool
                True if the contents of the current certificates are
                different from the fullchain. False otherwise.
        """

        path = self.certs_root_path + name + '/fullchain.pem'
        try:
            with open(path, 'r') as f:
                return f.read() != fullchain
        except FileNotFoundError:
            return True

    def lower_key(self, in_dict):
        if type(in_dict) is dict:
            out_dict = {}
            for key, item in in_dict.items():
                out_dict[key.lower()] = self.lower_key(item)
            return out_dict
        elif type(in_dict) is list:
            return [self.lower_key(obj) for obj in in_dict]
        else:
            return in_dict
