# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    tls-certs-monitor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tls-certs-monitor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tls-certs-monitor.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

import os
import logging
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler


logger = logging.getLogger(__name__)

class FileWatcher:
    def __init__(self, file_path, handler):
        self.observer = Observer()
        abs_path, file_name = os.path.split(os.path.abspath(file_path))
        self.observed_path = abs_path
        self.observed_file = file_name
        self.handler = handler

        self.event_handler = PatternMatchingEventHandler(
            patterns=['*/' + file_name],
            ignore_patterns=[],
            ignore_directories=True)

        self.event_handler.on_any_event = self._on_any_event

        self.observer.schedule(self.event_handler,
            self.observed_path,
            recursive=False)
        
    def _on_any_event(self, event):
        self.handler()

    def start(self):
        logger.debug('Watching file "%s" in directory "%s"', self.observed_file, self.observed_path)
        self.observer.start()

    def stop(self):
        self.observer.stop()
        self.observer.join()