#!/usr/bin/env python3

# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    tls-certs-monitor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tls-certs-monitor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tls-certs-monitor.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

'''tls_certs_monitor.py

This script monitors an ACME JSON certificate file compliant with
Treafik's format to update certificates of other services not handled by
Traefik.
Each time this file changes, it checks if the certificates of the services
it is reponsible for need updating. If so, it updates them and runs an action
on the docker container running the service to have the service reload the
new certificate. The action and th services that need to be managed are identified
by docker labels. This script monitors the docker socket to be informed when
services that need to be handled start or stop. Services can thus be added on
the fly, without restarting the script.
'''

import sys
import signal
import getopt
import time
import logging
from cert_updater import CertUpdater
from file_watcher import FileWatcher
from docker_monitor import DockerMonitor
from service_manager import ServicesManager


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def usage(cmd):
    eprint('TLS Certificates monitor - Monitors Traefik certificates used for non-Traefik-proxied services\n')
    eprint('Usage: {0} [options]\n'.format(cmd))
    eprint('Options:')
    eprint('  -h, --help                     shows this help')
    eprint('  -a, --acme-file=<file>         specifies the acme file to watch. Default is ./acme.json')
    eprint('  -o, --output-dir=<directory>   defines the output directory. Defaut is .')
    eprint('  -l, --loglevel=level           defines the logging level. Values are DEBUG, INFO, WARNING, ERROR, CRITICAL. Default is INFO.')
    eprint()

def exit_handler(signal, frame):
    raise(SystemExit)

def main(argv):
    acme_file = './acme.json'
    output_dir = '.'
    loglevel = 'INFO'

    try:
        opts, args = getopt.getopt(argv[1:], 'ha:o:l:', ['acme-file=', 'output-dir=', 'loglevel='])
    except getopt.GetoptError:
        usage(argv[0])
        sys.exit(1)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage(argv[0])
            sys.exit()
        elif opt in ('-a', '--acme-file'):
            acme_file = arg
        elif opt in ('-o', '--output-dir'):
            output_dir = arg
        elif opt in ('-l', '--loglevel'):
            loglevel = arg
            if arg not in ['DEBUG', 'INFO', 'ERROR', 'WARNING', 'ERROR', 'CRITICAL']:
                eprint('Invalid loglevel.')
                usage(argv[0])
                sys.exit(1)
        else:
            eprint('Invalid option {0}'.format(opt))
            usage(argv[0])
            sys.exit(1)

    # Setup logging
    logger = logging.getLogger()
    logger.setLevel(loglevel)
    formatter = logging.Formatter("[%(asctime)s][%(levelname)s]: %(message)s")
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    updater = CertUpdater(acme_file, output_dir)
    svc_mgr = ServicesManager(updater)
    file_watcher = FileWatcher(acme_file, svc_mgr.update)
    monitor = DockerMonitor(svc_mgr)

    logger.info("Starting TLS Certificates monitor")
    file_watcher.start()
    monitor.start()

    signal.signal(signal.SIGTERM, exit_handler)

    try:
        while True:
            time.sleep(10)
    except (KeyboardInterrupt, SystemExit):
        logger.info("Terminating.")
        monitor.stop()
        file_watcher.stop()

if __name__ == '__main__':
    main(sys.argv)
