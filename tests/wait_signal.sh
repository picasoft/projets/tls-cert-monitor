#!/bin/sh

# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    tls-certs-monitor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tls-certs-monitor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tls-certs-monitor.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

trap 'echo SIGHUP; exit 2' HUP
trap 'echo SIGTERM; exit 3' TERM
trap 'echo SIGUSR1; exit 4' USR1

echo 'WAITING'

while true; do sleep 1; done

exit 5

