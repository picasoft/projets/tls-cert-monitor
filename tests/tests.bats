#!/usr/bin/env bats

# Copyright 2019 S. Bonnet, Picasoft, Compiègne, France. All rights reserved.
#
# This file is part of tls-certs-monitor.
#
#    tls-certs-monitor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tls-certs-monitor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tls-certs-monitor.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors
#    Stéphane Bonnet <stephane.bonnet@hds.utc.fr>

# Helper functions
function start_container() {
    for cid in "$@"; do
        docker start $cid &> /dev/null
    done
}

function stop_container() {
    for cid in "$@"; do
        docker stop $cid &> /dev/null
    done
}

function remove_container() {
    for cid in "$@"; do
        docker rm $cid &> /dev/null
    done
}

function run_containers() {
    TEST_IMG_CID=$(docker run $@ --name tls-certs-client-test -d -l "traefik.frontend.rule=Host:test.example.net" -l "tls-certs-monitor.enable=true" $TEST_IMAGE_NAME) &> /dev/null
    IMG_CID=$(docker run -d --name tls-certs-client -e "log_level=DEBUG" -v /var/run/ -v ${VOLUMES}:/certs -v ${VOLUMES}:/output -v ${DOCKER_HOST}:/var/run/docker.sock:rw $IMAGE_NAME)
    sleep 2
}

function stop_containers() {
    stop_container $TEST_IMG_CID $IMG_CID
    remove_container $TEST_IMG_CID $IMG_CID
}

function make_volumes() {
    rm -rf $VOLUMES && mkdir -p $VOLUMES/test.example.net && chmod g+sw $VOLUMES/test.example.net &> /dev/null
    cp ${BATS_TEST_DIRNAME}/acme.json $VOLUMES/acme.json
}

function make_volumes_traefik_v2() {
    rm -rf $VOLUMES && mkdir -p $VOLUMES/test.example.net && chmod g+sw $VOLUMES/test.example.net &> /dev/null
    cp ${BATS_TEST_DIRNAME}/acme_traefik_v2.json $VOLUMES/acme.json
}

function remove_volumes() {
    rm -rf -- "$VOLUMES"
}

# Test harness setup / teardown
function setup() {
    IMAGE_NAME="$NAME:$VERSION"
    TEST_IMAGE_NAME="${NAME}_client:$VERSION"
    DOCKER_HOST=${DOCKER_HOST:-/var/run/docker.sock}
    VOLUMES=/tmp/VOLUMES
}

function teardown() {
    remove_volumes
    stop_container
    remove_container
}

# Test cases
@test "Image build" {
    run docker build -t $IMAGE_NAME $BATS_TEST_DIRNAME/..
    [ "$status" -eq 0 ]

    run docker build -t $TEST_IMAGE_NAME $BATS_TEST_DIRNAME
    [ "$status" -eq 0 ]

}

@test "Certificate creation" {
    # For Traefik v1
    make_volumes
    run_containers

    # Check the private key file has the proper permissions
    run stat -c "%a" $VOLUMES/test.example.net/privkey.pem
    [ "$output" -eq "600" ]

    # Check the proper certificates are created
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/cert.pem $VOLUMES/test.example.net/cert.pem
    [ "$status" -eq 0 ]
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/chain.pem $VOLUMES/test.example.net/chain.pem
    [ "$status" -eq 0 ]
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    stop_containers

    # For Traefik v2
    make_volumes_traefik_v2
    run_containers

    # Check the private key file has the proper permissions
    run stat -c "%a" $VOLUMES/test.example.net/privkey.pem
    [ "$output" -eq "600" ]

    # Check the proper certificates are created
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/cert.pem $VOLUMES/test.example.net/cert.pem
    [ "$status" -eq 0 ]
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/chain.pem $VOLUMES/test.example.net/chain.pem
    [ "$status" -eq 0 ]
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    stop_containers
}

@test "Certificate update" {
    # Traefik v1
    make_volumes
    run_containers

    # Check the initial certificates
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    # Update acme.json
    cp ${BATS_TEST_DIRNAME}/acme_update.json $VOLUMES/acme.json &> /dev/null
    sleep 1

    # Check the new cettificates
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain_update.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    stop_containers

    # Traefik v2
    make_volumes
    run_containers

    # Check the initial certificates
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    # Update acme.json
    cp ${BATS_TEST_DIRNAME}/acme_update_traefik_v2.json $VOLUMES/acme.json &> /dev/null
    sleep 1

    # Check the new cettificates
    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain_update.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    stop_containers
}

@test "Restart action" {
    make_volumes
    run_containers
    stop_containers

    run_containers -l "tls-certs-monitor.action=restart"

    cp ${BATS_TEST_DIRNAME}/acme_update.json $VOLUMES/acme.json &> /dev/null
    sleep 2

    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain_update.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    run docker logs $TEST_IMG_CID
    [ "${lines[1]}" == "SIGTERM" ]
    [ "${lines[2]}" == "WAITING" ]

    stop_containers
}

@test "Default kill action" {
    make_volumes
    run_containers
    stop_containers

    run_containers -l "tls-certs-monitor.action=kill"

    cp ${BATS_TEST_DIRNAME}/acme_update.json $VOLUMES/acme.json &> /dev/null
    sleep 2

    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain_update.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    run docker logs $TEST_IMG_CID
    [ "${lines[1]}" == "SIGHUP" ]

    stop_containers
}

@test "User-defined kill action" {
    make_volumes
    run_containers
    stop_containers

    run_containers -l "tls-certs-monitor.action=kill:SIGUSR1"

    cp ${BATS_TEST_DIRNAME}/acme_update.json $VOLUMES/acme.json &> /dev/null
    sleep 2

    run diff -qr $BATS_TEST_DIRNAME/test.example.net/fullchain_update.pem $VOLUMES/test.example.net/fullchain.pem
    [ "$status" -eq 0 ]

    docker logs $TEST_IMG_CID
    run docker logs $TEST_IMG_CID
    [ "${lines[1]}" == "SIGUSR1" ]

    stop_containers
}
