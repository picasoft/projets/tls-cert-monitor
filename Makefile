NAME=pica/tls-certs-monitor
VERSION=v1.3

.PHONY: all build build-nocache tag-latest test

all: build tag-latest

build:
	docker build -t $(NAME):$(VERSION) --rm .

tag-latest:
	docker tag $(NAME):$(VERSION) $(NAME):latest

build-nocache:
	docker build -t $(NAME):$(VERSION) --no-cache --rm .

test:
	@env NAME=$(NAME) VERSION=$(VERSION) bats tests/tests.bats
