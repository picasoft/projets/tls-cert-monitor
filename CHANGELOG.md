# v1.8

Avoid monitoring the same container multiple times.

# v1.7

Fixes for Traefik v2 and updated tests.

# v1.6

Compatibility with Traefik v2.

# v1.5

Fix Traefik URL parsing for `Path`

# v1.4

Allow the user to specify the owner's UID of certificates/keys files.

# v1.3

Take wildcard certificates into account.

# v1.2

Allow use of True/False for enabling tls-certs-monitor. This fixes boolean in Docker Compose being converted to a string with first letter uppercase, when dictionary style is used for labels.

# v1.1

Make label name consistent with tool name (`tls-certs-monitor`)

# v1.0

Initial version
