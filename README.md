# TLS Certs monitor

Ce service permet l'exploitation des certificats TLS générés par Traefik (v1 ou v2) par d'autres serveurs pour leur propres besoins de chiffrement de connexions.

Traefik a la capacité d'automatiser la demande et le renouvellement de certificats
TLS auprès de services en ligne tels que [Let's Encrypt](https://letsencrypt.org)
pour assurer le chiffrement des connexions HTTPS aux sites web dont il a la charge.

L'idée est de déléguer à Traefik la gestion de certificats utilisés
par d'autres services sans rapport avec lui, ce qui présente les avantages suivants :

* Les certificats générés sont signés par une autorité de certification publique dont
les certificats sont déjà présents sur la plupart des systèmes, facilitant ainsi la
vérification de l'authenticité des certificats par rapport à des certificats
auto-signés (pas de certificats de signature à déployer).
* A leur expiration, les certificats sont automatiquement renouvelés,
ce qui élimine au tâche d'administration et garantit la disponibilité
continue des services qui les utilisent.

Traefik v2 a la capacité de gérer le TLS des services non web, mais :
* La configuration est loin d'être triviale
* Traefik a besoin de l'extension `HostSNI` pour router vers des services non-web, ce que beaucoup de clients n'implémentent pas (par exemple pour LDAP)
* De façon générale, il est plus simple de laisser les services gérer les extensions TLS de leurs protocoles (SMTPS, LDAPS, etc), tandis que Traefik sait gérer HTTPS.

## Principe

Traefik maintient un cache de tous les certificats et clés provées TLS dans un fichier JSON, par défaut `acme.json`.
`tls_cert_monitor.py` surveille dans ce fichier la mise à jour par Traefik des certificats et clés pour les services dont il doit maintenir
les certificats. S'ils ont changé, il les extrait dans un répertoire portant le nom d'hôte
associé au service sous la forme suivante :

* `cert.pem` : certificat TLS du serveur
* `chain.pem` : certificat intermédiaire de l'autorité ayant signé le certificat du serveur (ici Let's Encrypt)
* `fullchain.pem` : chaîne complète comprenant le certificat TLS et le certificat intermédiaire
* `privatekey.pem` : clé privée du serveur

Il suffit de configurer le service pour qu'il utilise explicitement ces certificats.

Les services à maintenir sont identifiés par des étiquettes dans les images Docker. Le script surveille les démarrages
et arrêts de ces services en écoutant le socket Docker, ce qui lui permet de s'autoconfigurer.
Il peut aussi déclencher des actions sur les services après mise à jour des certificats. En effet, la plupart des serveurs ne les lisent qu'au démarrage ou doivent être forcés à les recharger au moyen de signaux.

`tls_cert_monitor` supporte ces deux mécanismes. Il est capable, en fonction de la
configuration du service, de soit le redémarrer (redémarrage du conteneur), soit lui
transmettre un signal défini par l'utilisateur (par défaut SIGHUP).
Cette configuration se fait également au moyen d'étiquettes associées aux conteneurs
des services.

## Construction et installation

L'application Python [`tls_cert_monitor.py`](app/tls_cert_monitor.py) peut être exécutée directement.
Pour faciliter sont déploiement, une image Docker légère basée sur [Alpine Linux](https://alpinelinux.org/) est également fournie (voir le [Dockerfile](Dockerfile)).
Pour la construire, dans le répertoire où le dépôt à été cloné,
exécuter simplement `make`.

Le script est configuré pour utiliser les fichiers et répertoires suivants dans
l'image:

* `/certs/acme.json` : fichier des certificats Traefik
* `/output/` : répertoire dans lequel les certificats seront extraits.

Ils peuvent être changés dans le Dockerfile si besoin est en modifiant les variables
d'environnement suivantes :

| Variable| Définition |
|---------|------------|
| `acme_path`| Cache des certificats Traefik |
| `cert_root`| Emplacement des certificats de sortie |
| `log_level`| Niveau d'affichage des messages |

## Tests

Les tests utilisent Bats (Bash Automated Testing System).

> [https://github.com/sstephenson/bats](https://github.com/sstephenson/bats)

Installez Bats et dans le répertoire du projet exécutez:

    make test

## Utilisation

Pour que le conteneur ait accès au fichier `acme.json` de Traefik et qu'il puisse
mettre à jour les certificats des services, il faut utiliser des volumes qui
permettront l'interaction entre ces trois entités. Tous les exemples utilisent
`docker-compose`.

### Configurer Traefik

La seule configuration nécessaire est de rendre disponible le fichier `acme.json`. Il
suffit de l'exporter vers l'hôte au travers d'un volume (à adapter en fonction de la
configuration locale de Traefik).

```yaml
  services:
    [...]

    traefik:
      image: traefik
      [...]
      volumes:
        - /my/host/acme.json:acme.json
        [...]
```

### Configurer un service

Pour configurer un service, il faut :

* Exporter ses certificats vers un répertoire de l'hôte
* Activer sa gestion par Traefik, **bien qu'il ne s'agisse pas d'un service web**
* Activer sa gestion par TLS-certs-monitor

#### Activation de la gestion d'un service par Traefik

Il suffit d'ajouter au service deux labels Traefik : `traefik.frontend.rule` et `traefik.enable`.

La seule règle frontend nécessaire est la règle `Host`. Elle est nécessaire pour
donner le nom canonique (et les noms alternatifs éventuels) du serveur qui sera
configuré dans le certificat. Dans l'exemple ci-dessous, le serveur a pour nom canonique `main.example.net` et pour nom alternatif `alternate1.example.net`.
Il faut aussi activer traefik pour ce service par `traefik.enable`.

```yaml
  services:
    [...]
    my-service:
      [...]
      labels:
        - "traefik.frontend.rule=Host:main.example.org,alternate1.example.org"
        - "traefik.enable=true"
```

Les règles de ports Traefik sont inutiles dans ce cas.

> **Attention** : le répertoire dans lequel les certificats sont extraits porte le
nom canonique du serveur (ici, main.example.org).

#### Activation de la gestion d'un service par TLS-certs-monitor

Le service TLS-certs-monitor utilise deux labels pour identifier les services dont
il doit s'occuper et l'action éventuelle à réaliser sur le conteneur quand les
certificats changent : `tls-certs-monitor.enable` et `tls-certs-monitor.action`.

> **Attention** Il ne peut y avoir qu'une seule action par service !

* Le label `tls-certs-monitor.enable` permet d'activer (valeur `true`) ou
de désactiver (valeur `false`) la gestion des certificats de ce service par
TLS-certs-monitor.

* Le label `tls-certs-monitor.action` permet de spécifier l'action à réaliser sur le
conteneur si les certificats associés changent. Les actions possibles sont les suivantes :
  * `restart` redémarre le conteneur du service
  * `kill` émet un signal vers le conteneur du service. Le signal par défaut est
     `SIGHUP`, mais n'importe quel autre signal peut être spécifié par la syntaxe
     `kill:signal_name`. Pour envoyer SIGUSR1 par exemple, l'action à spécifier est
     `kill:SIGUSR1`.

* *Optionnellement*, le label `tls-certs-monitor.owner` permet de spécifier l'UID du propriétaire
des fichiers créés (certificats, clés). Il prend pour valeur un entier. Par défaut,
le l'UID sera celui de l'utilisateur utilisé par le conteneur (`root`). Attention, toute personne
possédant cet UID sur l'hôte sera capable de lire la clé privée ; ce cas est à réserver si la
clé privéee est montée dans un conteneur ne s'exécutant pas en tant que `root`.

L'exemple suivant active la gestion des certificats de `my-service`, qui cherche
ses certificats dans le répertoire `/certs` de l'image et configure l'envoi
du signal `SIGUSR2` en cas de changement.

```yaml
    services:
    [...]
    my-service:
      [...]
      volumes:
        - /my/host/certs/main.example.org/:/certs

      labels:
        - "traefik.frontend.rule=Host:main.example.org,alternate1.example.org"
        - "traefik.enable=true"
        - "tls-certs-monitor.enable=true"
        - "tls-certs-monitor.action=kill:SIGUSR1"
        - "tls-certs-monitor.owner=999"
```
