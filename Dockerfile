FROM alpine:3.9

WORKDIR /usr/src/app
COPY requirements.txt ./

RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools -r requirements.txt && \
    rm -r /root/.cache \
    && mkdir /certs

ENV acme_file "/certs/acme.json"
ENV cert_root "/output"
ENV log_level "INFO"

COPY app/*.py ./

ENTRYPOINT python3 ./tls_certs_monitor.py --acme-file=${acme_file} --output-dir=${cert_root} --loglevel=${log_level}
